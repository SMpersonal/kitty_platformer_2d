#ifndef LEVEL_H
#define LEVEL_H
#include <string>
#include "player.h"
#include <windows.h>
#include <iostream>
#include "graphics.h"
#include <tgmath.h>
#include <map>
#include "bitmap_manager.h"
using namespace std;
#define PRESSED(vk_code) ((GetAsyncKeyState(vk_code) & 0x8000) ? 1 : 0)
class Level
{
    BitmapManager bitmapManager;
    string sLevel;
    size_t nLevelWidth;
    size_t nLevelHeight;
    size_t nTileWidth;
    size_t nTileHeight;
    Player player;
    float fCameraPosX = 0;
    float fCameraPosY = 0;

    const float fVel = 2.0;

    char getTile(int x, int y)
    {
        if (x >= 0 && x < nLevelWidth && y >= 0 && y < nLevelHeight)
            return sLevel[y * nLevelWidth + x];
        else
            return ' ';
    }

    void setTile(int x, int y, wchar_t c)
    {
        if (x >= 0 && x < nLevelWidth && y >= 0 && y < nLevelHeight)
            sLevel[y*nLevelWidth + x] = c;
    }

    public:
    void init(size_t nLevelWidthA, size_t nLevelHeightA, size_t nTileWidthA, size_t nTileHeightA, const string& sLevelA, const Player& playerA, const string& filePath)
    {
        nLevelWidth = nLevelWidthA;
        nLevelHeight = nLevelHeightA;
        nTileWidth = nTileWidthA;
        nTileHeight = nTileHeightA;
        sLevel = sLevelA;
        player = playerA;
        bitmapManager.readConfig(filePath);
    }
    void checkInput(float fElapsedTime);
    void doPositionCalculations(float fElapsedTime);
    void draw(HWND hwnd);
};
#endif