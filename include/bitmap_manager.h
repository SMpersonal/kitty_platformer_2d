#ifndef BITMAP_MANAGER_H
#define BITMAP_MANAGER_H
#include <string>
#include <iostream>
#include <fstream>
#include <exception>
#include <windows.h>
#include <map>
using namespace std;
class BitmapManager
{
    map<string, HBITMAP> bitmaps;
    public:
    void readConfig(string filePath);
    HBITMAP getBitmap(const string& symbol);
    ~BitmapManager()
    {
        for(auto& pair : bitmaps)
        {
            DeleteObject(pair.second);
        }
    }
};
#endif