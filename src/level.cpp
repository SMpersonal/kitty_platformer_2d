#include "level.h"

void Level::checkInput(float fElapsedTime)
{
    if(PRESSED(VK_UP))
    {
        player.fPlayerVelY += -fVel * fElapsedTime;
    }
    if(PRESSED(VK_DOWN))
    {
        player.fPlayerVelY += fVel * fElapsedTime;
    }
    if(PRESSED(VK_RIGHT))
    {
        player.fPlayerVelX += (player.bPlayerOnGround ? fVel : fVel/2.0f) * fElapsedTime;
    }
    if(PRESSED(VK_LEFT))
    {
        player.fPlayerVelX += (player.bPlayerOnGround ? -fVel : -fVel/2.0f) * fElapsedTime;
    }
    if(PRESSED(VK_SPACE))
    {
        if(player.fPlayerVelY == 0)
        {
            player.fPlayerVelY = -4.0f;
        }
    }
}

void Level::doPositionCalculations(float fElapsedTime)
{
    player.fPlayerVelY += 3.0f * fElapsedTime;
    if(player.bPlayerOnGround)
    {
        player.fPlayerVelX += (-fVel/1.5) * player.fPlayerVelX * fElapsedTime;
        if(fabs(player.fPlayerVelX) < 0.15f)
            player.fPlayerVelX = 0.0f;
    }
    //clamp velocities
    if (player.fPlayerVelX > 10.0f)
        player.fPlayerVelX = 10.0f;

    if (player.fPlayerVelX < -10.0f)
        player.fPlayerVelX = -10.0f;

    if (player.fPlayerVelY > 3.0f)
        player.fPlayerVelY = 3.0f;

    if (player.fPlayerVelY < -10.0f)
        player.fPlayerVelY = -10.0f;

    float fNewPlayerPosX = player.fPlayerPosX + player.fPlayerVelX * fElapsedTime;
    float fNewPlayerPosY = player.fPlayerPosY + player.fPlayerVelY * fElapsedTime; 

    if (getTile(fNewPlayerPosX + 0.0f, fNewPlayerPosY + 0.0f) == 'o')
        setTile(fNewPlayerPosX + 0.0f, fNewPlayerPosY + 0.0f, '.');

    if (getTile(fNewPlayerPosX + 0.0f, fNewPlayerPosY + 1.0f) == 'o')
        setTile(fNewPlayerPosX + 0.0f, fNewPlayerPosY + 1.0f, '.');

    if (getTile(fNewPlayerPosX + 1.0f, fNewPlayerPosY + 0.0f) == 'o')
        setTile(fNewPlayerPosX + 1.0f, fNewPlayerPosY + 0.0f, '.');

    if (getTile(fNewPlayerPosX + 1.0f, fNewPlayerPosY + 1.0f) == 'o')
        setTile(fNewPlayerPosX + 1.0f, fNewPlayerPosY + 1.0f, '.');

    //Collision
    if(player.fPlayerVelX <= 0)
    {
        // the reason why we have two checks here is because our player position is calculated from the upper left corner, so we need to also check for block below us to check if we are maybe colliding with that too
        // also here we use old player Y position because we wish to seperate X and Y
        if(getTile(fNewPlayerPosX + 0.0f, player.fPlayerPosY + 0.0f) != '.' || getTile(fNewPlayerPosX + 0.0f, player.fPlayerPosY + 0.9f) != '.')
        {
            fNewPlayerPosX = (int) fNewPlayerPosX + 1;
            player.fPlayerVelX = 0;
        }
    }
    else
    {
        //reason why we are adding 1 here is again because our player is calculated from the top left corner
        if(getTile(fNewPlayerPosX + 1.0f, player.fPlayerPosY + 0.0f) != '.' || getTile(fNewPlayerPosX + 1.0f, player.fPlayerPosY + 0.9f) != '.')
        {
            fNewPlayerPosX = (int) fNewPlayerPosX;
            player.fPlayerVelX = 0;
        }
    }
    player.bPlayerOnGround = false;
    if(player.fPlayerVelY <= 0)
    {
        //here we don't have to seperate X and Y cuz X was already checked
        if(getTile(fNewPlayerPosX + 0.0f, fNewPlayerPosY + 0.0f) != '.' || getTile(fNewPlayerPosX + 0.9f, fNewPlayerPosY + 0.0f) != '.')
        {
            fNewPlayerPosY = (int) fNewPlayerPosY + 1;
            player.fPlayerVelY = 0;
        }
    }
    else
    {
        //reason why we are adding 1 here is again because our player is calculated from the top left corner
        if(getTile(fNewPlayerPosX + 0.0f, fNewPlayerPosY + 1.0f) != '.' || getTile(fNewPlayerPosX + 0.9f, fNewPlayerPosY + 1.0f) != '.')
        {
            fNewPlayerPosY = (int) fNewPlayerPosY;
            player.fPlayerVelY = 0;
            player.bPlayerOnGround = true;
        }
    }
    player.fPlayerPosX = fNewPlayerPosX;
    player.fPlayerPosY = fNewPlayerPosY;


    fCameraPosX = player.fPlayerPosX;
    fCameraPosY = player.fPlayerPosY;
}

void Level::draw(HWND hwnd)
{
    RECT rect;
    GetClientRect(hwnd, &rect);
    HDC hdc = GetDC(hwnd);
    HDC hbufferdc = CreateCompatibleDC(hdc);
    HDC hmemorydc = CreateCompatibleDC(hdc);
    HBITMAP hmemorybitmap = CreateCompatibleBitmap(hdc, rect.right, rect.bottom);
    SelectObject(hmemorydc, hmemorybitmap);

    int nVisibleTilesX = rect.right / nTileWidth;
    int nVisibleTilesY = rect.bottom / nTileHeight;

    // Calculate Top-Leftmost visible tile
    float fOffsetX = fCameraPosX - (float)nVisibleTilesX / 2.0f;
    float fOffsetY = fCameraPosY - (float)nVisibleTilesY / 2.0f;

    // Clamp camera to game boundaries
    if (fOffsetX < 0) fOffsetX = 0;
    if (fOffsetY < 0) fOffsetY = 0;
    if (fOffsetX > nLevelWidth - nVisibleTilesX) fOffsetX = nLevelWidth - nVisibleTilesX;
    if (fOffsetY > nLevelHeight - nVisibleTilesY) fOffsetY = nLevelHeight - nVisibleTilesY;
    // std::cout << "rect" << rect.bottom << std::endl;
    // std::cout << "rect2" << rect.right << std::endl;
    // std::cout << "offset Y = " << fOffsetY << std::endl;
    // std::cout << "nLevel " << nLevelHeight << std::endl;
    // std::cout << "nvisible " << nVisibleTilesY << std::endl;

    // Get offsets for smooth movement
    float fTileOffsetX = (fOffsetX - (int)fOffsetX) * nTileWidth; // foffsetX - (int)foffsetX gives the fractional part
    float fTileOffsetY = (fOffsetY - (int)fOffsetY) * nTileHeight;
    for (int x = 0; x < nVisibleTilesX; x++)
    {
        for (int y = 0; y < nVisibleTilesY; y++)
        {
            wchar_t sTileID = getTile(x + fOffsetX, y + fOffsetY);
            switch (sTileID)
            {
                case '.' :
                    fillWithBitmap(x * nTileWidth - fTileOffsetX, y * nTileHeight - fTileOffsetY, (x+1) * nTileWidth - fTileOffsetX, (y + 1) * nTileHeight - fTileOffsetY, bitmapManager.getBitmap("."), &hmemorydc, &hbufferdc);
                    break;
                case 'o' :
                    fillWithBitmap(x * nTileWidth - fTileOffsetX, y * nTileHeight - fTileOffsetY, (x+1) * nTileWidth - fTileOffsetX, (y + 1) * nTileHeight - fTileOffsetY, bitmapManager.getBitmap("o"), &hmemorydc, &hbufferdc);
                    break;
                case ' ' :
                    std::cout << "returned ' ' tile " << std::endl;
                    std::cout << "returned ' ' tile " << std::endl;
                    std::cout << "returned ' ' tile " << std::endl;
                    std::cout << "returned ' ' tile " << std::endl;
                    std::cout << "returned ' ' tile " << std::endl;
                    break;
                default :
                    fillWithBitmap(x * nTileWidth - fTileOffsetX, y * nTileHeight - fTileOffsetY, (x+1) * nTileWidth - fTileOffsetX, (y + 1) * nTileHeight - fTileOffsetY, bitmapManager.getBitmap("else"), &hmemorydc, &hbufferdc);
                    break;
            }
        }

    }
    //draw player
    fillWithBitmap((player.fPlayerPosX - fOffsetX) * nTileWidth, (player.fPlayerPosY - fOffsetY) * nTileHeight, (player.fPlayerPosX - fOffsetX + 1) * nTileWidth, (player.fPlayerPosY - fOffsetY + 1) * nTileHeight, bitmapManager.getBitmap("player"), &hmemorydc, &hbufferdc);
    //std::cout << "player x :" << fPlayerPosX - fOffsetX  << "player y" << fPlayerPosY - fOffsetY << std::endl ;
    //clean up
    BitBlt(hdc, 0, 0, rect.right, rect.bottom, hmemorydc, 0, 0, SRCCOPY);
    DeleteDC(hbufferdc);
    DeleteDC(hmemorydc);
    DeleteObject(hmemorybitmap);
    ReleaseDC(hwnd, hdc);
}