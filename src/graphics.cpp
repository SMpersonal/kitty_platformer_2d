#include "graphics.h"
void fillWithBitmap(int x1, int y1, int x2, int y2, HBITMAP bitmap, HDC* hmemorydc, HDC* hbufferdc)
{
    SelectObject(*hbufferdc, bitmap);
    BitBlt(*hmemorydc, x1, y1, 64, 64, *hbufferdc, 0, 0, SRCCOPY);
}