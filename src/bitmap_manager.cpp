#include "bitmap_manager.h"
void BitmapManager::readConfig(string filePath)
{
    ifstream fHandle(filePath);
    std::string bitmaskSymbol, bitmaskFileName;
    if(fHandle.is_open())
    {
        while(fHandle >> bitmaskSymbol >> bitmaskFileName)
        {
            bitmaps[bitmaskSymbol] = (HBITMAP)LoadImage(NULL, ("./bitmaps/" + bitmaskFileName).c_str(), IMAGE_BITMAP, 0, 0, LR_LOADFROMFILE);
            if(bitmaps[bitmaskSymbol] == NULL)
            {
                throw invalid_argument("bitmap file");
            }
        }
    }
    else
    {
        throw invalid_argument("filePath");
    }
    fHandle.close();
}
HBITMAP BitmapManager::getBitmap(const string& symbol)
{
    return bitmaps[symbol];
}